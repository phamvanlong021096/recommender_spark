import os
import math

from pyspark import SparkContext, SparkConf
from pyspark.mllib.recommendation import ALS
from time import time

datasets_path = os.path.join('D:\data', 'movie_len')
complete_dataset_path = os.path.join(datasets_path, 'ml-latest.zip')
sc = SparkContext(appName="Pi")

# load the complete dataset file
complete_rating_file = os.path.join(datasets_path, 'ml-latest', 'ratings.csv')
complete_ratings_raw_data = sc.textFile(complete_rating_file)
complete_ratings_raw_data_header = complete_ratings_raw_data.take(1)[0]

# parse
complete_ratings_data = complete_ratings_raw_data.filter(lambda line: line != complete_ratings_raw_data_header) \
    .map(lambda line: line.split(",")).map(lambda tokens: (int(tokens[0]), int(tokens[1]), float(tokens[2]))).cache()

print("There are %s recommendations in the complete dataset" % (complete_ratings_data.count()))

# load the complete movies file
complete_movies_file = os.path.join(datasets_path, 'ml-latest', 'movies.csv')
complete_movies_raw_data = sc.textFile(complete_movies_file)
complete_movies_raw_data_header = complete_movies_raw_data.take(1)[0]

# Parse
complete_movies_data = complete_movies_raw_data.filter(lambda line: line != complete_movies_raw_data_header) \
    .map(lambda line: line.split(",")).map(lambda tokens: (int(tokens[0]), tokens[1], tokens[2])).cache()
complete_movies_titles = complete_movies_data.map(lambda x: (int(x[0]), x[1]))

print("There are %s movies in the complete dataset" % (complete_movies_titles.count()))


def get_counts_and_averages(ID_and_ratings_tuple):
    nratings = len(ID_and_ratings_tuple[1])
    return ID_and_ratings_tuple[0], (nratings, float(sum(x for x in ID_and_ratings_tuple[1])) / nratings)


movie_ID_with_ratings_RDD = (complete_ratings_data.map(lambda x: (x[1], x[2])).groupByKey())
movie_ID_with_avg_ratings_RDD = movie_ID_with_ratings_RDD.map(get_counts_and_averages)
movie_rating_counts_RDD = movie_ID_with_avg_ratings_RDD.map(lambda x: (x[0], x[1][0]))

# add new user ratings
new_user_ID = 0

# The format of each line is (userID, movieID, rating)
new_user_ratings = [
    (0, 260, 4),  # Star Wars (1977)
]
new_user_ratings_RDD = sc.parallelize(new_user_ratings)
print('New user ratings: %s' % new_user_ratings_RDD.take(10))

# add to data
complete_data_with_new_ratings_RDD = complete_ratings_data.union(new_user_ratings_RDD)

print("Number rating data", complete_ratings_data.count())
print("Number rating data new user", complete_data_with_new_ratings_RDD.count())

# train the ALS model
t0 = time()
best_rank = 8
seed = 5
iterations = 10
regularization_parameter = 0.1
new_ratings_model = ALS.train(complete_data_with_new_ratings_RDD, best_rank, seed=seed, iterations=iterations,
                              lambda_=regularization_parameter)
tt = time() - t0
print("New model trained in %s second" % round(tt, 3))

# getting top recommendations
new_user_ratings_ids = map(lambda x: x[1], new_user_ratings)  # get just movie IDs
# keep just those not on the ID list
new_user_unrated_movies_RDD = (
    complete_movies_data.filter(lambda x: x[0] not in new_user_ratings_ids).map(lambda x: (new_user_ID, x[0])))
# Use the input RDD, new_user_unrated_movies_RDD, with new_ratings_model.predictAll()
new_user_recommendations_RDD = new_ratings_model.predictAll(new_user_unrated_movies_RDD)

# Transform new_user_recommendations_RDD into pairs of the form (Movie ID, Predicted Rating)
new_user_recommendations_rating_RDD = new_user_recommendations_RDD.map(lambda x: (x.product, x.rating))
new_user_recommendations_rating_title_and_count_RDD = \
    new_user_recommendations_rating_RDD.join(complete_movies_titles).join(movie_rating_counts_RDD)
print(new_user_recommendations_rating_title_and_count_RDD.take(3))

new_user_recommendations_rating_title_and_count_RDD = \
    new_user_recommendations_rating_title_and_count_RDD.map(lambda r: (r[1][0][1], r[1][0][0], r[1][1]))

top_movies = new_user_recommendations_rating_title_and_count_RDD \
    .filter(lambda r: r[2] >= 25).takeOrdered(25, key=lambda x: -x[1])

print('TOP recommended movies (with more than 25 reviews):\n%s' %
      '\n'.join(map(str, top_movies)))
