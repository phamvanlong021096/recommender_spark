import os
import math

from pyspark import SparkContext, SparkConf
from pyspark.mllib.recommendation import ALS

datasets_path = os.path.join('D:\data', 'movie_len')
complete_dataset_path = os.path.join(datasets_path, 'ml-latest.zip')
sc = SparkContext(appName="Pi")

# load the complete dataset file
complete_rating_file = os.path.join(datasets_path, 'ml-latest', 'ratings.csv')
complete_ratings_raw_data = sc.textFile(complete_rating_file)
complete_ratings_raw_data_header = complete_ratings_raw_data.take(1)[0]

# parse
complete_ratings_data = complete_ratings_raw_data.filter(lambda line: line != complete_ratings_raw_data_header) \
    .map(lambda line: line.split(",")).map(lambda tokens: (int(tokens[0]), int(tokens[1]), float(tokens[2]))).cache()

print("There are %s recommendations in the complete dataset" % (complete_ratings_data.count()))

# train the recommender model
best_rank = 8
seed = 5
iterations = 10
regularization_parameter = 0.1
training_RDD, test_RDD = complete_ratings_data.randomSplit([7, 3], seed=0)
complete_model = ALS.train(training_RDD, best_rank, seed=seed, iterations=iterations, lambda_=regularization_parameter)

# test on testing set
test_for_predict_RDD = test_RDD.map(lambda x: (x[0], x[1]))
predicttions = complete_model.predictAll(test_for_predict_RDD).map(lambda r: ((r[0], r[1]), r[2]))
rates_and_preds = test_RDD.map(lambda r: ((int(r[0]), int(r[1])), float(r[2]))).join(predicttions)
error = math.sqrt(rates_and_preds.map(lambda r: (r[1][0] - r[1][1]) ** 2).mean())

print('For testing data the RMSE is %s' % error)
