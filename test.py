import os
import logging

from time import time
from pyspark.mllib.recommendation import ALS
from pyspark import SparkContext, SparkConf

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def init_spark_context():
    # load spark context
    conf = SparkConf().setAppName("movie_recommendation_server")
    # Important: pass aditional Python modules to each worker
    sc = SparkContext(appName="Pi")

    return sc


def get_counts_and_averages(ID_and_ratings_tuple):
    """
    Given a tuple (movieID, ratings_iterable)
    returns (movieID, (ratings_count, ratings_avg))
    """
    nratings = len(ID_and_ratings_tuple[1])

    return ID_and_ratings_tuple[0], (nratings, float(sum(x for x in ID_and_ratings_tuple[1])) / nratings)


sc = init_spark_context()
dataset_path = os.path.join('D:\data', 'movie_len', 'ml-latest')

# load the complete dataset file
complete_rating_file = os.path.join(dataset_path, 'ratings.csv')
complete_ratings_raw_data = sc.textFile(complete_rating_file)
complete_ratings_raw_data_header = complete_ratings_raw_data.take(1)[0]

# parse
complete_ratings_data = complete_ratings_raw_data.filter(lambda line: line != complete_ratings_raw_data_header) \
    .map(lambda line: line.split(",")).map(
    lambda tokens: (int(tokens[0]), int(tokens[1]), float(tokens[2]))).cache()

print("There are %s recommendations in the complete dataset" % (complete_ratings_data.count()))

# load the complete movies file
complete_movies_file = os.path.join(dataset_path, 'movies.csv')
complete_movies_raw_data = sc.textFile(complete_movies_file)
complete_movies_raw_data_header = complete_movies_raw_data.take(1)[0]

# Parse
complete_movies_data = complete_movies_raw_data.filter(lambda line: line != complete_movies_raw_data_header) \
    .map(lambda line: line.split(",")).map(lambda tokens: (int(tokens[0]), tokens[1], tokens[2])).cache()
complete_movies_titles = complete_movies_data.map(lambda x: (int(x[0]), x[1]))

print("There are %s movies in the complete dataset" % (complete_movies_titles.count()))

# __count_and_average_ratings()

movie_ID_with_ratings_RDD = (complete_ratings_data.map(lambda x: (x[1], x[2])).groupByKey())
movie_ID_with_avg_ratings_RDD = movie_ID_with_ratings_RDD.map(get_counts_and_averages)
movie_rating_counts_RDD = movie_ID_with_avg_ratings_RDD.map(lambda x: (x[0], x[1][0]))

t0 = time()
best_rank = 8
seed = 5
iterations = 10
regularization_parameter = 0.1
new_ratings_model = ALS.train(complete_ratings_data, best_rank, seed=seed, iterations=iterations,
                              lambda_=regularization_parameter)
tt = time() - t0
print("New model trained in %s second" % round(tt, 3))
