import os

from engine import RecommendationEngine
from pyspark import SparkContext, SparkConf


def init_spark_context():
    # load spark context
    conf = SparkConf().setAppName("movie_recommendation_server")
    # Important: pass aditional Python modules to each worker
    sc = SparkContext(conf=conf)

    return sc


def main():
    sc = init_spark_context()
    dataset_path = os.path.join('D:\data', 'movie_len', 'ml-latest-small')
    recommendation_engine = RecommendationEngine(sc, dataset_path)


if __name__ == '__main__':
    main()
