import os
import argparse
import time
import gc

from pyspark.sql import SparkSession, Row
from pyspark.sql.functions import col, lower
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.recommendation import ALS


class AlsRecommender(object):
    """
    This a collaborative filtering recommender with Alternating Least Square
    Matrix Factorization, which is implemented by Spark
    """

    def __init__(self, spark_session, path_movies, path_ratings):
        self.spark = spark_session
        self.sc = spark_session.sparkContext
        self.moviesDF = self._load_file(path_movies).select(['movieId', 'title'])
        self.ratingsDF = self._load_file(path_ratings).select(['userId', 'movieId', 'rating'])
        self.model = ALS(userCol='userId', itemCol='movieId', ratingCol='rating', coldStartStrategy='drop')

    def _load_file(self, filepath):
        """
        load csv file into memory as spark DF
        """
        return self.spark.read.load(filepath, format='csv', header=True, inferSchema=True)

    def set_model_params(self, maxIter, regParam, rank):
        """
        set model params for pyspark.ml.recommendation.ALS
        :param maxIter: int, max number of learning iterations
        :param regParam: float, regularization parameter
        :param rank: float, number of latent factors
        """
        self.model = self.model.setMaxIter(maxIter).setRank(rank).setRegParam(regParam)

    def make_recommendations(self, fav_movie, n_recommendations):
        """
        make top n movie recommendations
        :param fav_movie: str, name of user input movie
        :param n_recommendations: int, top n recommendations
        """
        # make inference and get raw recommendations
        print('Recommendation system start to make inference...')
        t0 = time.time()
        raw_recommends = self._inference(self.model, fav_movie, n_recommendations)

    def _inference(self, model, fav_movie, n_recommendations):
        """
        return top n movie recommendations based on user's input movie
        :param model: spark ALS model
        :param fav_movie: str, name of user input movie
        :param n_recommendations: int, top n recommendations
        :return: list of top n similar movie recommendations
        """


def parse_args():
    parser = argparse.ArgumentParser(prog="Movie Recommender", description="Run ALS Movie Recommender")
    parser.add_argument('--path', nargs='?', default='../data/MovieLens', help='input data path')
    parser.add_argument('--movies_filename', nargs='?', default='movies.csv', help='provide movies filename')
    parser.add_argument('--ratings_filename', nargs='?', default='ratings.csv', help='provide ratings filename')
    parser.add_argument('--movie_name', nargs='?', default='', help='provide your favorite movie name')
    parser.add_argument('--top_n', type=int, default=10, help='top n movie recommendations')
    return parser.parse_args()


if __name__ == '__main__':
    # get args
    args = parse_args()
    data_path = args.path
    movies_filename = args.movies_filename
    ratings_filename = args.ratings_filename
    movie_name = args.movie_name
    top_n = args.top_n

    # initial spark
    spark = SparkSession.builder.appName("movie recommender").getOrCreate()

    # initial recommender system
    recommender = AlsRecommender(spark,
                                 os.path.join(data_path, movies_filename),
                                 os.path.join(data_path, ratings_filename))

    # set params
    recommender.set_model_params(10, 0.05, 20)

    # make recommendations
    recommender.make_recommendations(movie_name, top_n)

    # stop
    spark.stop()
